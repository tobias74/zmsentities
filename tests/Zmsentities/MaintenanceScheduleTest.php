<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests;

use BO\Zmsentities\MaintenanceSchedule;

class MaintenanceScheduleTest extends EntityCommonTests
{
    public $entityclass = '\BO\Zmsentities\MaintenanceSchedule';

    public $collectionclass = '\BO\Zmsentities\Collection\MaintenanceScheduleList';

    public function testBasics()
    {
        /** @var MaintenanceSchedule $entity */
        $entity = (new $this->entityclass())->getExample();

        self::assertIsInt($entity['id']);
        self::assertIsInt($entity['creatorId']);
        self::assertNotNull($entity['creationDateTime']);
        self::assertIsBool($entity['isActive']);
        self::assertIsBool($entity['isRepetitive']);
        self::assertIsString($entity['timeString']);
        self::assertIsInt($entity['duration']);
        self::assertIsString($entity['area']);
        self::assertIsString($entity['documentBody']);

        self::assertSame(9876, $entity->getId());
        self::assertSame(138, $entity->getCreatorId());
        self::assertSame('2015-12-31 12:34:00', $entity->getCreationDateTime()->format('Y-m-d H:i:s'));
        self::assertSame('2016-04-01 23:30:00', $entity->getStartDateTime()->format('Y-m-d H:i:s'));
        self::assertSame(false, $entity->isActive());
        self::assertSame(true, $entity->isRepetitive());
        self::assertSame('30 23 * * *', $entity->getTimeString());
        self::assertSame(60, $entity->getDuration());
        self::assertSame('zms*', $entity->getArea());
        self::assertStringContainsString('Systemwartung', $entity->getAnnouncement());
        self::assertStringContainsString('Wartung', $entity->getDocumentBody());

        $json = '{';
        $json .=  '"$schema":"https:\\/\\/schema.berlin.de\\/queuemanagement\\/maintenanceSchedule.json",';
        $json .=  '"creatorId":138,';
        $json .=  '"creationDateTime":"' . $entity['creationDateTime']->format(DATE_ATOM) . '",';
        $json .=  '"startDateTime":"2016-04-01T23:30:00+02:00",';
        $json .=  '"isActive":false,';
        $json .=  '"isRepetitive":true,';
        $json .=  '"timeString":"30 23 * * *",';
        $json .=  '"duration":60,';
        $json .=  '"leadTime":20,';
        $json .=  '"area":"zms*",';
        $json .=  '"announcement":"<p class=\u0022message message--success\u0022>';
        $json .=  '<strong>Geplante Systemwartung<\/strong><br><br>';
        $json .=  'Um {{ maintenance.startDateTime|date(\'H:i\') }} Uhr ist eine Systemwartung geplant. ';
        $json .=  'Die Terminbuchung ist in dem Zeitraum nicht m&ouml;glich.<\/p>",';
        $json .=  '"documentBody":"<h1>{% trans %}Wartung{% endtrans %}<\/h1>';
        $json .=    '{% trans %}Die Terminverwaltung wird momentan gewartet.{% endtrans %} ';
        $json .=    '{% trans %}Bitte probieren Sie es zu einem sp&auml;teren Zeitpunkt erneut.{% endtrans %}<br>';
        $json .=    '{% trans %}Vielen Dank f&uuml;r Ihr Verst&auml;ndnis.{% endtrans %}';
        $json .=  '","id":9876';
        $json .= '}';

        self::assertSame($json, $entity->__toString());
    }
}
