<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Collection;

use BO\Zmsentities\ApplicationRegister;
use BO\Zmsentities\Collection\ApplicationRegisterList;
use BO\Zmsentities\Schema\Factory;
use PHPUnit\Framework\TestCase;

class ApplicationRegisterListTest extends TestCase
{
    public function testExport(): void
    {
        $now = new \DateTime('2001-01-01 12:34:56');
        $collection = new ApplicationRegisterList([new ApplicationRegister([
            'id' => '00000000-0000-0000-0000-000000000000',
            'type' => 'ticketprinter',
            'parameters' => '/ticketprinter/scope/123/',
            'startDate' => $now,
            'lastDate'  => $now,
        ])]);

        $body = json_encode($collection, JSON_UNESCAPED_SLASHES);

        $expected = '[{';
        $expected .=   '"$schema":"https://schema.berlin.de/queuemanagement/applicationRegister.json",';
        $expected .=   '"type":"ticketprinter",';
        $expected .=   '"parameters":"/ticketprinter/scope/123/",';
        $expected .=   '"userAgent":null,';
        $expected .=   '"scopeId":null,';
        $expected .=   '"startDate":"2001-01-01 12:34:56",';
        $expected .=   '"lastDate":"2001-01-01",';
        $expected .=   '"daysActive":1,';
        $expected .=   '"id":"00000000-0000-0000-0000-000000000000"';
        $expected .=   '}]';

        self::assertSame($expected, $body);

        $data   = json_decode($body, true);
        $entity = Factory::create(reset($data))->getEntity();

        self::assertSame($entity->parameters, '/ticketprinter/scope/123/');
        self::assertInstanceOf(\DateTimeInterface::class, $entity->startDate);
        self::assertInstanceOf(\DateTimeInterface::class, $entity->lastDate);
        self::assertSame('2001-01-01', $entity->startDate->format('Y-m-d'));
    }
}