<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Collection;

use BO\Zmsentities\Collection\MaintenanceScheduleList;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule;
use PHPUnit\Framework\TestCase;

class MaintenanceScheduleListTest extends TestCase
{
    public function testMaintenanceScheduleList()
    {
        $list = new MaintenanceScheduleList();

        $entity1 = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString'   => '2098-12-12 12:34:56',
            'duration'     => 30
        ]);

        $list->addEntity($entity1);

        $entity2 = new MaintenanceSchedule([
            'isRepetitive' => true,
            'timeString'   => '5 0 * * 7',
            'duration'     => 30
        ]);

        $list->addEntity($entity2);

        $entity3 = new MaintenanceSchedule([
            'isRepetitive' => false,
            'timeString'   => '2096-12-12 12:34:56',
            'duration'     => 30
        ]);

        $list->addEntity($entity3);

        $list->sortByScheduleTime();
        $listArray = (array)$list;
        $nextEntry = reset($listArray);

        self::assertNull($list->getActiveEntry());
        self::assertSame('5 0 * * 7', $nextEntry->getTimeString());

        $entity4 = new MaintenanceSchedule([
            'isActive'     => true,
            'isRepetitive' => true,
            'timeString'   => '*/2 * * * *',
            'duration'     => 30
        ]);

        $list->addEntity($entity4);

        self::assertInstanceOf(MaintenanceSchedule::class, $list->getActiveEntry());

        $list->sortByScheduleTime(DateTime::create('2021-12-21 01:00:00'));
        $listArray = (array)$list;
        $nextEntry = reset($listArray);

        self::assertSame('*/2 * * * *', $nextEntry->getTimeString());
    }
}