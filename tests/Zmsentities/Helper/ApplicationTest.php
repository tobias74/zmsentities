<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests\Helper;

use BO\Zmsentities\Helper\Application as ApplicationHelper;
use BO\Zmsentities\Workstation;
use BO\Zmsentities\Scope;
use PHPUnit\Framework\TestCase;

class ApplicationTest extends TestCase
{
    public function testGetRandomHashByWorkstation(): void
    {
        $workstation = new Workstation();
        $workstation->getScope()->offsetSet(Scope::PRIMARY, 123);

        $hash    = ApplicationHelper::getRandomHashByWorkstation($workstation);
        $scopeId = ApplicationHelper::getScopeIdByHash($hash);

        self::assertSame(123, $scopeId);
    }
}