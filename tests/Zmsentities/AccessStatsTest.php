<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Tests;

use BO\Zmsentities\AccessStats;

class AccessStatsTest extends EntityCommonTests
{
    public $entityclass = '\BO\Zmsentities\AccessStats';

    public $collectionclass = '\BO\Zmsentities\Collection\AccessStatsList';

    public function testBasics()
    {
        /** @var AccessStats $entity */
        $entity = (new $this->entityclass())->getExample();
        self::assertIsInt($entity['id']);
        self::assertIsInt($entity['lastActive']);
        self::assertIsString($entity['role']);
        self::assertIsString($entity['location']);

        self::assertSame(9876, $entity->getId());
        self::assertSame('user', $entity->getRole());
        self::assertSame(1234567890, $entity->getLastActive());
        self::assertSame('zmsadmin', $entity->getLocation());
    }
}
