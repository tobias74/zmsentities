<?php

namespace BO\Zmsentities\Tests;

use Ramsey\Uuid\Uuid;

class ProcessArchiveTest extends EntityCommonTests
{
    public $entityclass = '\BO\Zmsentities\ProcessArchive';

    public $collectionclass = '\BO\Zmsentities\Collection\ProcessArchiveList';

    public function testBasic()
    {
        $entity = $this->getExample();
        $uuidString = $entity::getUuid();
        $this->assertEquals(32, strlen($uuidString));
    }
}
