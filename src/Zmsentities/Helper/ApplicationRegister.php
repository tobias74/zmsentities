<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Helper;

use BO\Zmsentities\Calldisplay;
use BO\Zmsentities\ApplicationRegister as Entity;

class ApplicationRegister
{
    public static function getEntityByCalldisplay(Calldisplay $calldisplay): Entity
    {
        $regEntity = new Entity();
        $regEntity->id = $calldisplay->hash;
        $regEntity->type = Entity::TYPE_ZMS2_CALLDISPLAY;
        $regEntity->scopeId = Application::getScopeIdByHash($calldisplay->hash);

        if (!$regEntity->scopeId && $calldisplay->hasScopeList()) {
            $regEntity->scopeId = $calldisplay->getScopeList()->getIds()[0];
        }
        if (!empty($calldisplay->status)) {
            $status = $calldisplay->status;
            $regEntity->userAgent = $status['agent'] ?? null;
            $parameters = [
                'collections' => [],
                'queue'       => (strlen($status['list'] ?? '') > 0) ? ['status' => $status['list']] : null,
                'template'    => $status['template'] ?? null,
                'qrcode'      => $status['qrcode'] ?? null,
            ];
            if ($calldisplay->hasScopeList()) {
                $parameters['collections']['scopelist'] = implode(',', $calldisplay->getScopeList()->getIds());
            }
            if ($calldisplay->hasClusterList()) {
                $parameters['collections']['clusterlist'] = implode(',', $calldisplay->getClusterList()->getIds());
            }
            $regEntity->parameters = urldecode(http_build_query($parameters));
        }

        return $regEntity;
    }
}
