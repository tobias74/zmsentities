<?php

namespace BO\Zmsentities\Helper;

use BO\Zmsentities\Workstation;

class Application
{
    public static function getRandomHashByWorkstation(Workstation $workstation): string
    {
        $hash = md5(random_int(9999999, PHP_INT_MAX));
        $scopeId = str_pad((string) $workstation->getScope()->getId(), 5, '0', STR_PAD_LEFT);
        $alphabet = '0123456789' . implode(range('A', 'Z')) . implode(range('a', 'z')); //uri allowed chars
        $secret = md5('I love Berlin');

        if (class_exists('\App') && property_exists('\App', 'urlSignatureSecret')) {
            $secret = md5(\App::$urlSignatureSecret);
        }

        $lastpos = strpos($alphabet, $secret[hexdec($hash[0])]);
        for ($s = 0; $s < strlen($scopeId); $s++) {
            $position = strpos($alphabet, $scopeId[$s]);
            for ($i = 0; $i < strlen($hash); $i++) {
                $position += strpos($alphabet, $hash[$i]) + strpos($alphabet, $secret[$i]) + $lastpos;
            }
            $lastpos = $position = ($position % strlen($alphabet));
            $scopeId[$s] = $alphabet[$position];
        }

        return 'hbw' . $hash . $scopeId;
    }

    public static function getScopeIdByHash(string $hash): ?int
    {
        if (str_starts_with($hash, 'hbw')) {
            $scopeId = substr($hash, -5);
            $hash = substr($hash, 3, -5);
            $alphabet = '0123456789' . implode(range('A', 'Z')) . implode(range('a', 'z')); //uri allowed chars
            $secret = md5('I love Berlin');
            if (class_exists('\App') && property_exists('\App', 'urlSignatureSecret')) {
                $secret = md5(\App::$urlSignatureSecret);
            }

            $lastpos = strpos($alphabet, $secret[hexdec($hash[0])]);
            for ($s = 0; $s < strlen($scopeId); $s++) {
                $position = strpos($alphabet, $scopeId[$s]);
                for ($i = 0; $i < strlen($hash); $i++) {
                    $position -= strpos($alphabet, $hash[$i]) + strpos($alphabet, $secret[$i]) + $lastpos;
                }
                $position = (($position % strlen($alphabet)) + strlen($alphabet)) % strlen($alphabet);
                $lastpos = strpos($alphabet, $scopeId[$s]);
                $scopeId[$s] = $alphabet[$position];
            }

            return (int) $scopeId;
        }

        return null;
    }
}
