<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities\Collection;

use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule;
use BO\Zmsentities\Helper\MaintenanceSchedule as MaintenanceScheduleHelper;
use DateTimeImmutable;

class MaintenanceScheduleList extends Base
{
    const ENTITY_CLASS = '\BO\Zmsentities\MaintenanceSchedule';

    /**
     * this currently sorts by using the end time to include ongoing maintenance
     */
    public function sortByScheduleTime(?\DateTimeInterface $nowTime = null): MaintenanceScheduleList
    {
        $nowTime = $nowTime ?? DateTime::create();

        $this->uasort(function (?MaintenanceSchedule $a, ?MaintenanceSchedule $b) use ($nowTime) {
            $time1 = MaintenanceScheduleHelper::getNextRunEnd($a, $nowTime);
            $time2 = MaintenanceScheduleHelper::getNextRunEnd($b, $nowTime);
            if (!$time1 instanceof DateTimeImmutable) {
                return -1;
            }
            if (!$time2 instanceof DateTimeImmutable) {
                return 1;
            }

            return (int) $time1->format('U') - (int) $time2->format('U');
        });

        return $this;
    }

    public function getActiveEntry(): ?MaintenanceSchedule
    {
        /** @var MaintenanceSchedule $entry */
        foreach ($this as $entry) {
            if ($entry->isActive()) {
                return $entry;
            }
        }

        return null;
    }
}
