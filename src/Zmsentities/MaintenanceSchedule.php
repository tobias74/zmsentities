<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsentities;

use ArrayObject;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\Schema\UnflattedArray;

class MaintenanceSchedule extends Schema\Entity
{
    public const PRIMARY = 'id';

    public static $schema = "maintenanceSchedule.json";

    public function __construct($input = null, $flags = ArrayObject::ARRAY_AS_PROPS, $iterator_class = "ArrayIterator")
    {
        if (is_array($input) or $input instanceof ArrayObject) {
            $input = new UnflattedArray($input);
        }

        if ($input instanceof UnflattedArray) {
            $originalInput = new UnflattedArray($input->getValue());
            $originalInput->copyBoolean('isRepetitive', $input);
            $originalInput->copyBoolean('isActive', $input);
            $originalInput->copyInteger('duration', $input);
            $originalInput->copyDateTime('creationDateTime', $input);
            $originalInput->copyDateTime('startDateTime', $input);

            if ($originalInput->offsetStrlen('startDateTime') === 0) {
                $input->offsetSet('startDateTime', null);
            }

            if ($originalInput->offsetStrlen('timeString') > 0) {
                $input->offsetSet('timeString', trim($input->getValue()['timeString']), ' ');
            }
        }

        parent::__construct($input, $flags, $iterator_class);
    }

    public function getEntityName()
    {
        return 'maintenanceSchedule';
    }

    public function getDefaults()
    {
        $advice = '<p class="message message--success"><strong>Geplante Systemwartung</strong><br><br>';
        $advice .= "Um {{ maintenance.startDateTime|date('H:i') }} Uhr ist eine Systemwartung geplant. ";
        $advice .= "Die Terminbuchung ist in dem Zeitraum nicht m&ouml;glich.</p>";

        $body = '<h1>{% trans %}Wartung{% endtrans %}</h1>';
        $body .= '{% trans %}Die Terminverwaltung wird momentan gewartet.{% endtrans %} ';
        $body .= '{% trans %}Bitte probieren Sie es zu einem sp&auml;teren Zeitpunkt erneut.{% endtrans %}<br>';
        $body .= '{% trans %}Vielen Dank f&uuml;r Ihr Verst&auml;ndnis.{% endtrans %}';

        return [
            'creatorId'        => 0,
            'creationDateTime' => DateTime::create(),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => false,
            'timeString'       => '',
            'duration'         => 60,
            'leadTime'         => 20,
            'area'             => 'zms',
            'announcement'     => $advice,
            'documentBody'     => $body,
        ];
    }

    public function getCreatorId(): int
    {
        return $this['creatorId'];
    }

    public function setCreatorId(int $creatorId): void
    {
        $this['creatorId'] = $creatorId;
    }

    public function getCreationDateTime(): \DateTimeInterface
    {
        $dateTime = $this['creationDateTime'];

        return $dateTime instanceof \DateTimeInterface ? $dateTime : DateTime::create($dateTime);
    }

    public function setCreationDateTime(\DateTimeInterface $creationDateTime): void
    {
        $this['creationDateTime'] = $creationDateTime;
    }

    public function getStartDateTime(): ?\DateTimeInterface
    {
        $dateTime = $this['startDateTime'];

        return !is_string($dateTime) ? $dateTime : DateTime::create($dateTime);
    }

    public function setStartDateTime(?\DateTimeInterface $startDateTime): void
    {
        $this['startDateTime'] = $startDateTime;
    }

    public function isActive(): bool
    {
        return $this['isActive'];
    }

    public function setActive(bool $isActive): void
    {
        $this['isActive'] = $isActive;
    }

    public function isRepetitive(): bool
    {
        return $this['isRepetitive'];
    }


    public function getTimeString(): string
    {
        return $this['timeString'];
    }

    /**
     * returns amount of minutes
     */
    public function getDuration(): int
    {
        return $this['duration'];
    }

    /**
     * returns amount of minutes
     */
    public function getLeadTime(): int
    {
        return $this['leadTime'];
    }

    /**
     * returns the intended area to be affected
     */
    public function getArea(): string
    {
        return $this['area'];
    }

    /**
     * returns the html content to be displayed
     */
    public function getDocumentBody(): string
    {
        return $this['documentBody'];
    }
    /**
     * returns the html content to be displayed
     */
    public function getAnnouncement(): string
    {
        return $this['announcement'];
    }
}
