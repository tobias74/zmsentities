<?php
declare(strict_types=1);

namespace BO\Zmsentities;

class AccessStats extends Schema\Entity
{
    public const PRIMARY = 'id';

    public static $schema = "accessStats.json";

    public function getEntityName()
    {
        return 'accessStats';
    }

    public function getDefaults()
    {
        return [
            'id'         => 0,
            'role'       => 'user',
            'lastActive' => time(),
            'location'   => 'zmsadmin',
        ];
    }

    public function getRole(): string
    {
        return $this['role'];
    }

    /**
     * @return int (timestamp)
     */
    public function getLastActive(): int
    {
        return $this['lastActive'];
    }

    /**
     * @return string (zms component or else)
     */
    public function getLocation(): string
    {
        return $this['location'];
    }
}
