<?php
declare(strict_types=1);

namespace BO\Zmsentities;

use Ramsey\Uuid\Uuid;

/**
 * @property int createIP
 * @property int createTimestamp
 * @property int processId
 * @property int archiveId
 * @property int lastChange
 * @property string id
 * @property string status
 */
class ProcessArchive extends Schema\Entity
{
    const PRIMARY = 'id';

    public static $schema = "processArchive.json";

    /**
     * @return string
     */
    public function getEntityName(): String
    {
        return 'processArchive';
    }

    /**
     * @return array
     */
    public function getDefaults(): Array
    {
        return [
            'createIP' => '',
            'createTimestamp' => self::getCurrentTimestamp(),
            'id' => self::getUuid(),
            'processId' => 0,
            'archiveId' => 0,
            'status' => 'free',
            'lastChange' => self::getCurrentTimestamp()
        ];
    }

    /**
     * @return string uuid4
     */
    public static function getUuid(): String
    {
        return Uuid::uuid4()->getHex();
    }

    /**
     * @return string $string
     */
    public function __toString(): String
    {
        $string = "processArchive#";
        $string .= $this->id ?: $this->archiveId;
        $string .= " (" . $this->status . ")";
        $string .= " ~" . base_convert($this['lastChange'], 10, 35);
        return $string;
    }
}
