{
  "type": "object",
  "description": "A processArchive represents a request from one or more clients, usually including an appointment. Compared to an appointment, the process has additional information about the status of the request from the client.",
  "example": {
    "createIP": "145.15.3.10",
    "createTimestamp": 1447931596,
    "lastChange": 1447931596,
    "processId": 123456,
    "archiveId": 56123650,
    "id": "550e8400e29b41d4a716446655440000",
    "status": "reserved" 
  },
  "required": [
    "id",
    "processId",
    "archiveId"
  ],
  "additionalProperties": false,
  "properties": {
    "archiveId": {
      "type": "number",
      "description": "Number is used to identify a process in an archive (for statistical purposes)\n"
    },
    "processId": {
      "type": "number",
      "description": "Number is used to identify referenced process\n",
      "minimum": 1000
    },
    "createIP": {
      "type": "string",
      "description": "IP Address of the creator\n"
    },
    "createTimestamp": {
      "type": "number",
      "description": "unix timestamp representing creation of the process\n"
    },
    "id": {
      "type": "string",
      "description": "id as UUID is used to identify an archived process\n"
    },
    "lastChange": {
      "type": "number",
      "description": "unix timestamp of the last change on this process\n"
    },
    "status": {
      "type": "string",
      "enum": [
        "free",
        "reserved",
        "confirmed",
        "queued",
        "called",
        "processing",
        "pending",
        "pickup",
        "finished",
        "missed",
        "archived",
        "deleted",
        "anonymized",
        "blocked",
        "conflict"
      ],
      "description": "Status of the process. The following values are possible:\n  * free: open for reservation\n  * reserved: for updating data, no validation yet\n  * confirmed: appointment\n  * queued: confirmed and in waiting queue\n  * called: show on call displays\n  * processing: client appeared and gets service\n  * pending: client has to return to fetch papers\n  * pickup: client is called to fetch papers\n  * finished: successfully processed\n  * missed: no client appeared\n  * archived: only used in statistics\n  * deleted: blocking appointment time\n  * anonymized: for statistically purposes only\n  * blocked: to avoid the reuse of IDs on the same day\n  * conflict: mark process as conflict for check functions\n"
    }
  }
}
